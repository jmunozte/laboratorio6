package com.academiasmoviles.lab06.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.academiasmoviles.lab06.R
import com.academiasmoviles.lab06.modelo.Directorio
import kotlinx.android.synthetic.main.item_lista.view.*

class DirectorioAdapter(var registro: MutableList<Directorio>, val llamada: (item:Directorio) -> Unit) : RecyclerView.Adapter<DirectorioAdapter.DirectorioAdapterHolder>()  {


    class DirectorioAdapterHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    {

        fun bind(registro: Directorio, llamada: (item: Directorio) -> Unit)
        {
            itemView.tvNombre.text = registro.nombre
            itemView.tvProfesion.text = registro.profesion
            itemView.tvCorreo.text = registro.correo
            itemView.tvletra.text = registro.nombre.substring(0, 1)

            itemView.imgTel.setOnClickListener {
                llamada(registro)
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DirectorioAdapterHolder {
        val view :View = LayoutInflater.from(parent.context).inflate(R.layout.item_lista,parent, false)
        return DirectorioAdapterHolder(view)
    }

    override fun getItemCount(): Int {
       return registro.size
    }

    override fun onBindViewHolder(holder: DirectorioAdapterHolder, position: Int) {
        val registro =  registro[position]
        holder.bind(registro, llamada)

    }


}
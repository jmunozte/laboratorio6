package com.academiasmoviles.lab06

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.academiasmoviles.lab06.adapter.DirectorioAdapter
import com.academiasmoviles.lab06.modelo.Directorio
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    val registro = mutableListOf<Directorio>()
    lateinit var adaptador: DirectorioAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        cargarInformacion()
        configurarAdaptador()
    }

    private fun configurarAdaptador() {
        adaptador = DirectorioAdapter(registro){
          //  Toast.makeText(this,it.numero,Toast.LENGTH_SHORT).show()
            val num = it.numero
            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:" + it.numero)
            startActivity(intent)

        }
        recycleviewDirectorio.adapter = adaptador
        recycleviewDirectorio.layoutManager = LinearLayoutManager(this)
    }

    private fun cargarInformacion() {
        registro.add(Directorio("Jorge Muñoz","Ingeniero de Sistemas","jorgeluismunoz@hotmail.com","955347889"))
        registro.add(Directorio("Ana Flores","Docente","ana@gmail.com","444444444"))
        registro.add(Directorio("Alexander Vega","Contador","alexander@gmail.com","333333333"))
        registro.add(Directorio("Sadit Muñoz","Estudiante","sadit@gmail.com","222222222"))
        registro.add(Directorio("Carlos Romero","Asistente","carlos@gmail.com","111111111"))
    }


}